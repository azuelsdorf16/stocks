# coding: utf-8

# The purpose of this application is to download historical stock data from the NASDAQ &reg;, clean the data, compute statistics about each symbol's data, and determine which symbols are undervalued ("low") based on those statistics.

#Necessary imports
import pandas as pd
import os
import re
import numpy as np
import datetime
import dateutil
from Downloader import Downloader

def download_clean_save_stock_data(symbol, start_date, end_date, data_location):
    """
    Download, clean, and save stock data to disk.

    Keyword arguments:
    symbol -- string stock symbol for which we want to download data.
    start_date -- string first date at which we want our download to consider stock data. In format yyyy-mm-dd.
    end_date -- string last date at which we want our download to consider stock data. In format yyyy-mm-dd.
    """

    downloader = Downloader(20, 30, 60)

    stock_prices = downloader.download("https://www.nasdaq.com/api/v1/historical/{0}/etf/{1}/{2}".format(symbol.lower(), start_date, end_date), True)

    if stock_prices is None:
        return None

    #Remove NaN values from our stock prices. Sometimes, if it is a weekend, the NASDAQ will leave
    #a blank line in the first row.
        
    stock_prices.dropna(inplace=True)
      
    #If the date in the first record (today's date) is not in the format MM/DD/YYYY, such as is the
    #case when the data was last updated at 4PM on a trading day, then replace the date with the date
    #on which this script started. If the date in the first row is NaN, then remove the first row and
    #move on.
    
    #Date column is the first column in the dataframe (hence the first zero) and we care about the
    #first record in the dataframe (hence the second zero).
        
    if re.findall(r"([0-9]{2}\/[0-9]{2}\/[0-9]{4})", stock_prices.iloc[0, 0]) == []:
        stock_prices.iloc[0, 0] = today_date
        
        #Sometimes, there are records for the same day in the NASDAQ's data, particularly
        #the start of the current trading day and the end of the current trading day. By
        #making the substitution above, we may cause ourselves to have two records with the
        #same date. We will prefer the first one since it is the latest information.
        stock_prices.drop_duplicates(subset=["Date"], inplace=True, keep='first')

    #Save our data after combining it with any pre-existing data for the same symbol.
    data_path = os.path.join(data_location, "{0}.csv".format(symbol))
    
    if os.path.exists(data_path):
        old_stock_prices = pd.read_csv(data_path)

        stock_prices = pd.concat([stock_prices, old_stock_prices])

        stock_prices = stock_prices.drop_duplicates(subset=["Date"], keep='first').reset_index(drop=True)

    stock_prices.to_csv(data_path, index=False)

    return stock_prices

def main():
    years_window = 10 #How far back we want our downloads to go (in years)

    symbols = [
    "EDV",
    "BIV",
    "VGIT",
    "BLV",
    "VGLT",
    "VMBS",
    "BSV",
    "VTIP",
    "VGSH",
    "BND",
    "VCIT",
    "VCLT",
    "VCSH",
    "VTC",
    "VTEB",
    "VIG",
    "VUG",
    "VYM",
    "VV",
    "MGC",
    "MGK",
    "MGV",
    "VOO",
    "VTI",
    "VTV",
    "VXF",
    "VO",
    "VOT",
    "VOE",
    "VB",
    "VBK",
    "VBR",
    "VWOB",
    "VEU",
    "VSS",
    "VEA",
    "VWO",
    "VGK",
    "VPL",
    "VNQI",
    "VIGI",
    "VYMI",
    "BNDX",
    "VXUS",
    "VT",
    "VOX",
    "VCR",
    "VDC",
    "VDE",
    "VFH",
    "VHT",
    "VIS",
    "VGT",
    "VAW",
    "VNQ",
    "VPU"
    ] #The symbols of the stocks we are interested in.

    threshold = 3 #number of sigmas from the mean a stock's standard
    #score must fall below to be considered "undervalued".

    data_location = "data" #relative path to folder where our downloaded data will be stored.

    output_location = "output" #relative path to folder where our output will be stored.

    weeks_previous = 52 * 2 #Number of weeks to consider when filtering the data.

    download = True #Whether to download the latest set of data and add it to our
    #existing data set, if applicable.

    #Make sure the folder where we want to store the data exists.
    if not os.path.exists(data_location):
        os.makedirs(data_location)
    
    #Make sure the folder where we want to store our output exists.
    if not os.path.exists(output_location):
        os.makedirs(output_location)

    #Will be needed later on for datetime manipulations.
    today = datetime.datetime.today()

    #Download our data or load it from disk.
    symbol_data = {}

    for symbol in symbols:
        if download:
            end_date = today.strftime("%Y-%m-%d")
            start_date = (today - dateutil.relativedelta.relativedelta(years=years_window)).strftime("%Y-%m-%d")
            stock_prices = download_clean_save_stock_data(symbol, start_date, end_date, data_location)
        else:
            data_path = os.path.join(data_location, "{0}.csv".format(symbol))

            stock_prices = pd.read_csv(data_path)
 
        symbol_data[symbol] = stock_prices

    # Now we can compute the statistics. We will compute the sample standard
    # deviation and sample mean based on the close prices of each stock.

    value_data = {"Symbol" : [], "Latest Price" : [], "Mean Price" : [],
              "Price Stddev" : [], "Sigmas from mean" : [],
              "Undervalued" : [], "Percent Below" : []}

    previous_date = today - datetime.timedelta(weeks=weeks_previous)

    print("Assessing performance between {0} and {1}".format(previous_date, today))

    for symbol in symbols:
        #Filter data based on date range
        relevant_data = symbol_data[symbol].copy()
    
        relevant_data["Date"] = pd.to_datetime(relevant_data["Date"])
    
        relevant_data = relevant_data[(relevant_data["Date"] <= today) & (relevant_data["Date"] > previous_date)]
   
        close_prices = relevant_data[" Close/Last"]
    
        latest_price = close_prices[0]
        stddev = np.std(close_prices)
        mean = np.mean(close_prices)
    
        score = (latest_price - mean) / stddev
    
        value_data["Symbol"].append(symbol)
        value_data["Latest Price"].append(latest_price)
        value_data["Mean Price"].append(mean)
        value_data["Price Stddev"].append(stddev)
        value_data["Sigmas from mean"].append(score)
    
        if score < (-1 * threshold):
            value_data["Undervalued"] = "Yes"
            print("\"{0}\" is undervalued.".format(symbol))
        else:
            value_data["Undervalued"] = "No"
            print("\"{0}\" is not undervalued.".format(symbol))
        
        percent_below = 100. * len(relevant_data[relevant_data[" Close/Last"] < latest_price]) / len(relevant_data[" Close/Last"])   
    
        #Since we are using a sample from an empirical distribution, determine
        #what percent of the data is below our ETF's current price.
        value_data["Percent Below"].append(percent_below)

    value_data = pd.DataFrame(value_data)

    output_file_name = os.path.join(output_location, today.strftime("Analysis_%m_%d_%Y.csv"))

    print("Saving analysis to {0}".format(output_file_name))

    value_data.to_csv(output_file_name, index=False)

if __name__ == "__main__":
    main()

import unittest
from Analyzer import download_clean_save_stock_data
import time
import pandas as pd
import os
import shutil

class AnalyzerTests(unittest.TestCase):
    def test_download_new(self):
        temp_dir = "/tmp/{}".format(time.time())

        os.mkdir(temp_dir)

        stock_prices = download_clean_save_stock_data("VOX", "2019-01-02", "2020-01-03", temp_dir)

        old_stock_prices = pd.read_csv(os.path.join("test_data", "VOX_new_saved.csv"))

        expected_columns = ["Date"," Close/Last"," Volume"," Open"," High"," Low"]

        self.assertEqual(list(stock_prices.columns), expected_columns)
        self.assertEqual(list(old_stock_prices.columns), expected_columns)

        for column in stock_prices.columns:
            self.assertEqual(len(stock_prices[column]), len(stock_prices[column]))

            for index in range(len(stock_prices[column])):
                self.assertEqual(stock_prices[column][index], stock_prices[column][index])

    def test_download_existing(self):
        temp_dir = "/tmp/{}".format(time.time())

        os.mkdir(temp_dir)

        shutil.copyfile(os.path.join("test_data", "VOX_existing_saved.csv"),
            os.path.join(temp_dir, "VOX.csv"))

        old_stock_prices = pd.read_csv(os.path.join("test_data", "VOX_existing_saved.csv"))

        stock_prices = download_clean_save_stock_data("VOX", "2019-10-02", "2020-01-06", temp_dir)

        expected_columns = ["Date"," Close/Last"," Volume"," Open"," High"," Low"]

        self.assertEqual(list(stock_prices.columns), expected_columns)
        self.assertEqual(list(old_stock_prices.columns), expected_columns)

        for column in stock_prices.columns:
            self.assertEqual(len(stock_prices[column]), len(stock_prices[column]))

            for index in range(len(stock_prices[column])):
                self.assertEqual(stock_prices[column][index], stock_prices[column][index])

if __name__ == "__main__":
    unittest.main()

import grab
import random
import time
import pandas as pd
from io import StringIO

class Downloader(grab.Grab):
    def __init__(self, retries, retry_delay_min, retry_delay_max):
        """
        Constructor. Initializes instance variables.

        Keyword arguments:
        retries -- integer number of times to retry a download.
        retry_delay_min -- integer minimum seconds to delay before attempting to retry a download.
        retry_delay_max -- integer maximum seconds to delay before attempting to retry a download.
        """

        super(Downloader, self).__init__()

        if retries < 1:
            raise ValueError("Number of retries ({}) must be at least one.".format(retries))

        if retry_delay_min < 0:
            raise ValueError("Minimum retry delay ({}) must be at least zero.".format(retry_delay_min))

        if retry_delay_max < 0:
            raise ValueError("Minimum retry delay ({}) must be at least zero.".format(retry_delay_max))

        if retry_delay_min > retry_delay_max:
            raise ValueError("Minimum retry delay ({}) must be less than or equal to maximum retry delay ({}).".format(retry_delay_min, retry_delay_max))

        self.retries = retries
        self.retry_delay_min = retry_delay_min
        self.retry_delay_max = retry_delay_max

    def download(self, url, is_csv):
        """
        Downloads the content of a URL and converts it to a CSV if desired.

        Keyword arguments:
        url -- string the URL to download
        is_csv -- boolean whether to convert the downloaded content to a CSV or not.
        """

        attempt = 0

        while attempt < self.retries:
            try:
                response = self.go(url)

                if is_csv:
                    return pd.read_csv(StringIO(response.unicode_body()))
                else:
                    return StringIO(response.unicode_body())
            except KeyboardInterrupt as ke:
                print("Failed to get data for {0} due to keyboard interrupt.".format(url))
                return None
            except BaseException as be:
                delay = random.randint(self.retry_delay_min, self.retry_delay_max)
                attempt += 1

                print("Failed to get data for {0} due to {1}.".format(url, str(be)))

                if attempt < self.retries:
                    print("Retrying in {0} seconds...".format(delay))
                else:
                    print("Exceeded maximum retries ({}). Returning None.".format(self.retries))
                    return None

                time.sleep(delay)

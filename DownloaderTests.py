import unittest
from Downloader import Downloader

class DownloaderTests(unittest.TestCase):
    def test_init_valid(self):
        """Tests whether the download can handle valid constructor params"""
        download = Downloader(1, 0, 10)
        self.assertEqual(download.retries, 1)
        self.assertEqual(download.retry_delay_min, 0)
        self.assertEqual(download.retry_delay_max, 10)

    def test_init_invalid_1(self):
        """Tests whether the downloader can detect invalid constructor params"""
        try:
            downloader = Downloader(-1, 0, 10)
            self.assertFalse("Retries was -1")
        except ValueError as ve:
            self.assertTrue("Number of retries (-1) must be at least one." in str(ve))

    def test_init_invalid_2(self):
        """Tests whether the downloader can detect invalid constructor params"""
        try:
            downloader = Downloader(1, 1, 0)
            self.assertFalse("Invalid params (min 1, max 0)")
        except ValueError as ve:
            self.assertTrue(True)

    def test_init_invalid_3(self):
        """Tests whether the downloader can detect invalid constructor params"""
        try:
            downloader = Downloader(1, -1, 2)
            self.assertFalse("Invalid params (min -1)")
        except ValueError as ve:
            self.assertTrue(True)

    def test_init_invalid_4(self):
        """
        Tests whether the downloader can detect invalid constructor params. This case is currently
        redundant since the min is less than the maximum and we should have checks to catch that,
        but it is here in case someone removes that check.
        """

        try:
            downloader = Downloader(1, 1, -2)
            self.assertFalse("Invalid params (max -2)")
        except ValueError as ve:
            self.assertTrue(True)

if __name__ == "__main__":
    unittest.main()
